package Measurement;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 * Measurement.Measurement je rodicovska trida pro vsechny typy mereni ( v soucasne dobe typy A a B)
 */
public interface Measurement {

    /**
     * Metody jsou implementovany v potomcich rozhrani Measurement, podle jejich specifickych potreb
     */
    void readAndWrite() throws IOException;

    /**
     * Metoda nacte prvni validni (neprazdnou) rakdu ze vstupniho souboru a vyhodnoti ji
     * @return Objekt potomka tridy Measurement.Measurement, ktere souhlasi se zadanym typem ze vstupu
     * @throws IOException V pripade selhani cteni a zapisovani nebo neznameho typu mereni (validni pouze 'A' a 'B')
     */
    static Measurement getType(BufferedReader inputFile, BufferedWriter outputFile) throws IOException {
        String line;
        while( (line = inputFile.readLine()) != null){
            line = line.trim();
            if ( line.length() == 0 ) {
                continue;
            } else if ( line.length() != 1){
                throw new IllegalArgumentException("Nesprávné čtení typu měření");
            }
            break;
        }

        char type = line != null ? line.toUpperCase().charAt(0) : '0'; // v pripade nenacteni radky type = '0'
        switch ( type ) {
            case 'A': return new MeasurementA(inputFile, outputFile);
            case 'B': return new MeasurementB(inputFile, outputFile);
            default: throw new IllegalArgumentException(type + " -- Zadaný typ měření není validní.");
        }
    }
}
