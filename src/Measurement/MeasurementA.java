package Measurement;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import DataTypes.*;

/**
 * Trida mereni typu A, potomek rozhrani Measurement.Measurement
 */
public class MeasurementA implements Measurement {
    public final char type = 'A';
    private final BufferedReader inputFile;
    private final BufferedWriter outputFile;

    public MeasurementA(BufferedReader inputFile, BufferedWriter outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
    }

    /**
     * Metoda cte ze vstupniho souboru jednu radku po druhe, kazdou inhed vyhodnoti a zapise do vystupniho souboru
     * @throws IOException V pripade selhani cteni nebo zapisu
     */
    @Override
    public void readAndWrite() throws IOException {
        String line;
        outputFile.write(type + "\n"); // typ mereni
        while ( (line = inputFile.readLine()) != null){
            line = line.trim();
            if ( line.length() == 0 ) {  // preskoci prazdne radky
                continue;
            }
            DataType dataType;
            String result;
            try {
                 dataType = DataType.getType(line);  // vraci konkretni tridu, ktera je v souladu se zadanym labelem
                 result = dataType.resultToString(); // vraci naformatovany String
            } catch (IllegalArgumentException e){
                result = e.getMessage(); // v pripade nespravneho vstupu se vypise error message misto vysledku
                }
            outputFile.write( result + "\n"); // zapise formatovany string s vysledkem (nebo errorem) do vystupniho souboru
        }
    }
}
