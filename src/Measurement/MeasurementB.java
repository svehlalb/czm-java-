package Measurement;

import DataTypes.DataType;
import DataTypes.MinMaxDataType;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.TreeMap;

/**
 * Trida mereni typu B, potomek rozhrani Measurement.Measurement
 */
public class MeasurementB implements Measurement {
    public final char type = 'B';
    private final BufferedReader inputFile;
    private final BufferedWriter outputFile;
    private final TreeMap<String, MinMaxDataType> values;

    /**
     * Zavola se konstruktor rodice a je vyplnena TreeMap vsemi existujicimi labely jako klici
     * @param inputFile BufferedReader vstupniho souboru
     * @param outputFile BufferedWriter vystupniho souboru
     */
    public MeasurementB(BufferedReader inputFile, BufferedWriter outputFile) {
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        values = new TreeMap<>();
        for ( String label : DataType.labelTypes ){    // vyplni TreeMap vsemi znamymi labely jako klicy
            values.put(label, new MinMaxDataType(label));
        }
    }
    /**
     * Metoda cte ze vstupniho souboru jednu radku po druhe, data postupne uklada do TreeMap values
     * @throws IOException V pripade selhani cteni ze souboru
     */
    private void read( ) throws IOException {
        String line;
        outputFile.write(type + "\n"); // typ mereni
        while ( (line = inputFile.readLine()) != null) {
            line = line.trim();
            if (line.length() == 0) {  // preskoci prazdne radky
                continue;
            }
            DataType dataType;
            try {
                dataType = DataType.getType(line);  // vraci konkretni tridu, ktera je v souladu se zadanym labelem
                if ( values.get(dataType.getLabel()) == null )
                    throw new IllegalArgumentException( line + " -- Metody u této třídy nepřekrývají rodičovské metody");

                values.get(dataType.getLabel()).add(dataType); // najde prvek v TreeMap a zavola metodu add(DataType), ktera aktualizuje minimum a maximum daneho labelu
            } catch (IllegalArgumentException e){
                System.out.println(e.getMessage());
            }
        }

        }

    /**
     * Metoda zapise do vystupniho souboru formatovane minimum a maximum pro kazdy zadany label
     * @throws IOException V pripade selhani zapisu do souboru
     */
    private void write( ) throws IOException {
        for (MinMaxDataType dataType: values.values()) {
            String line = dataType.toString();
            if ( !line.equals("") ) // "" bude obrzen, pokud nebyl dany label soucasti vstupu
                outputFile.write( dataType.toString() + "\n");
        }
    }

    @Override
    public void readAndWrite() throws IOException {
        read();
        write();
    }
}
