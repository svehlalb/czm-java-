package DataTypes;

import java.util.ArrayList;

/**
 * Trida DataTyp reprezentuje rodicovske rozhrani vsech datovych typu (v soucasne dobe 6 typu)
 */
public interface DataType  {
    String[] labelTypes = { "bias", "distance", "flexibility", "frequency", "humidity", "speed" };

    /**
     * Metoda nacte prvni validni (neprazdnou) rakdu ze vstupniho souboru a vyhodnoti ji
     * @param line zkoumana radka
     * @return Objekt potomka tridy DataTyp, ktere souhlasi se zadanym typem ze vstupu
     * @throws IllegalArgumentException V pripade, kdy je nacten typ, ktery nebyl rozpoznan
     */
    static DataType getType(String line) throws IllegalArgumentException {
        String[] values = line.trim().split(" "); // odstrani prebytecne whitespacy a rozdeli Stringy jednotlive do pole
        switch (values[0].toLowerCase()){
            case "bias": return new Bias(line);
            case "distance": return new Distance(line);
            case "flexibility": return new Flexibility(line);
            case "frequency": return new Frequency(line);
            case "humidity": return new Humidity(line);
            case "speed": return new Speed(line);
            default: throw new IllegalArgumentException(values[0] + " -- Zadaný datový typ není validní.");
        }
    }

    /**
     * Metoda ziska ze radky hodnoty
     * @param numberOfArguments pocet hodnot daneho labelu
     * @return ArrayList<Double> extrahovanych hodnot
     * @throws IllegalArgumentException Pokud nesouhlasi pocet hodnot se zadanym poctem hodnot daneho labelu
     */
    static ArrayList<Double> extractData(String line, int numberOfArguments) throws IllegalArgumentException {
        String[] values = line.trim().replaceAll("\\s+"," ").split(" "); // odstrani prebytecne whitespacy a rozdeli Stringy jednotlive do pole
        if ( values.length != numberOfArguments + 1 )
            throw new IllegalArgumentException(line + " -- Počet hodnot se neshoduje se zadaným formátem");
        
        ArrayList<Double> extractedValues = new ArrayList<>();
        for ( int i = 1; i < numberOfArguments + 1; i++ ){
            extractedValues.add( Double.parseDouble(values[i]) ); // konvertuje string do double a prida do extractedValues
        }
        return extractedValues;
    }

    /**
     * Metoda ve tride implementujici DataType vrati vysledek operace ve formatovanem Stringu
     * @return formatovany String s vysledkem
     */
    String resultToString();

    /**
     * @return hodnota vysledku operace konkretniho DataTypu v double
     */
    double getResult();

    /**
     * @return String s labelem  konkretniho DataTypu
     */
    String getLabel();
}
