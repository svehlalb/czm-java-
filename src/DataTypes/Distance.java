package DataTypes;

import java.util.ArrayList;

/**
 * Trida reprezentuje datovy typ Distance, je potomek rozhrani DataType
 */
public class Distance implements DataType {
    private double result;
    private final String line;
    public final int numberOfValues = 2;
    public final String label = "distance";

    /**
     * Konstruktor zavola metodu compute(), aby ihned po vzniku byla promenna result spravne nastavena
     * @param line zpracovavana radka ze vstupu
     */
    public Distance(String line) {
        this.line = line;
        compute();
    }

    /**
     * Metoda provede specificky vypocet a nastavi promennou result
     */
    private void compute( ) {
        ArrayList<Double> values = DataType.extractData(line, numberOfValues);
        result =  values.get(0)/values.get(1); // d1/d2
    }

    @Override
    public String resultToString() {
        return label + " " + result;
    }

    @Override
    public double getResult() {
        return result;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
