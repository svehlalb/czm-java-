package DataTypes;

import java.util.ArrayList;

/**
 * Trida reprezentuje datovy typ Humidity, je potomek rozhrani DataType
 */
public class Humidity implements DataType {
    private double result;
    private final String line;
    public final int numberOfValues = 3;
    public final String label = "humidity";

    /**
     * Konstruktor zavola metodu compute(), aby ihned po vzniku byla promenna result spravne nastavena
     * @param line zpracovavana radka ze vstupu
     */
    public Humidity(String line) {
        this.line = line;
        compute( );
    }

    /**
     * Metoda provede specificky vypocet a nastavi promennou result
     */
    private void compute( ) {
        ArrayList<Double> values = DataType.extractData(line, numberOfValues);
        result = 1;
        for ( double v : values ) {
            result *= v;
        }
    }

    @Override
    public String resultToString() {
        return label + " " + result;
    }

    @Override
    public double getResult() {
        return result;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
