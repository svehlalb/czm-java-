package DataTypes;

import java.util.ArrayList;

/**
 * Trida reprezentuje datovy typ Frequency, je potomek rozhrani DataType
 */
public class Frequency implements DataType {
    private double result;
    private final String line;
    public final int numberOfValues = 4;
    public final String label = "frequency";

    /**
     * Konstruktor zavola metodu compute(), aby ihned po vzniku byla promenna result spravne nastavena
     * @param line zpracovavana radka ze vstupu
     */
    public Frequency(String line) {
        this.line = line;
        compute();
    }

    /**
     * Metoda provede specificky vypocet a nastavi promennou result
     */
    private void compute( ) {
        ArrayList<Double> values = DataType.extractData(line, numberOfValues);
        result = (values.get(0) + values.get(1) + values.get(2)) / values.get(3); // (f1 + f2 + f3)/f4
    }

    @Override
    public String resultToString() {
        return label + " " + result;
    }

    @Override
    public double getResult() {
        return result;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
