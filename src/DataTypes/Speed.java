package DataTypes;

import javax.xml.crypto.Data;
import java.util.ArrayList;

/**
 * Trida reprezentuje datovy typ Speed, je potomek rozhrani DataType
 */
public class Speed implements DataType {
    private double result;
    private final String line;
    public final int numberOfValues = 5;
    public final String label = "speed";

    /**
     * Konstruktor zavola metodu compute(), aby ihned po vzniku byla promenna result spravne nastavena
     * @param line zpracovavana radka ze vstupu
     */
    public Speed(String line) {
        this.line = line;
        compute();
    }

    /**
     * Metoda provede specificky vypocet a nastavi promennou result
     */
    private void compute( ) {
        ArrayList<Double> values = DataType.extractData(line, numberOfValues);
        result = (values.get(0) + values.get(1)) * (values.get(2) + values.get(3)); // (s1 + s2) * (s3 + s4)
        result /= values.get(4); // ((s1 + s2) * (s3 + s4)) / s5
    }

    @Override
    public String resultToString() {
        return label + " " + result;
    }

    @Override
    public double getResult() {
        return result;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
