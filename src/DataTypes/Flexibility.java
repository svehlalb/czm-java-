package DataTypes;

import java.util.ArrayList;

/**
 * Trida reprezentuje datovy typ Flexibility, je potomek rozhrani DataType
 */
public class Flexibility implements DataType {
    private double result;
    private final String line;
    public final int numberOfValues = 2;
    public final String label = "flexibility";

    /**
     * Konstruktor zavola metodu compute(), aby ihned po vzniku byla promenna result spravne nastavena
     * @param line zpracovavana radka ze vstupu
     */
    public Flexibility(String line) {
        this.line = line;
        compute();
    }

    /**
     * Metoda provede specificky vypocet a nastavi promennou result
     */
    private void compute( ) {
        ArrayList<Double> values = DataType.extractData(line, numberOfValues);
        result = (values.get(0)/2) * values.get(1); // (f1/2) / f2
    }

    @Override
    public String resultToString() {
        return label + " " + result;
    }

    @Override
    public double getResult() {
        return result;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
