package DataTypes;

import java.util.ArrayList;

/**
 * Trida reprezentuje datovy typ Bias, je potomek rozhrani DataType
 */
public class Bias implements DataType {
    private double result;
    private final String line;
    public final String label = "bias";
    public final int numberOfValues = 6;

    /**
     * Konstruktor zavola metodu compute(), aby ihned po vzniku byla promenna result spravne nastavena
     * @param line zpracovavana radka ze vstupu
     */
    public Bias(String line) {
        this.line = line;
        compute( );
    }

    /**
     * Metoda provede specificky vypocet a nastavi promennou result
     */
    private void compute( ) {
        ArrayList<Double> values = DataType.extractData(line, numberOfValues);
        result =  values.get(0) * values.get(1); // b1 * b2
        for( int i = 2; i < numberOfValues; i++ ){
            if ( i == 2 || i == 5 ) // + b3 + b6 .. 2 a 5 kvuli indexaci pole od 0
                result += values.get(i);
            else  // - b4 - b5
                result -= values.get(i);
        }
    }

    @Override
    public String resultToString() {
        return label + " " + result;
    }

    @Override
    public double getResult() {
        return result;
    }

    @Override
    public String getLabel() {
        return label;
    }
}
