package DataTypes;

/**
 * Trida slouzi k ulozeni minimalni a maximalni hodnoty od jednoho DataTypu, vyuziti v Measurement.MeasurementB
 */
public class MinMaxDataType {
    private DataType min;
    private DataType max;
    public final String label;

    public MinMaxDataType(String l){
        min = null;
        max = null;
        this.label = l;
    }

    /**
     * Metoda porovna vysledek vstupniho DataTypu s ulozenymi min a max a pripadne je nahradi
     * @param dataType porovnavany DataTyp
     */
    public void add(DataType dataType) {
        if (min == null && max == null){ // prvni porovnani, dataTyp je nastavan jako min i max
            min = dataType;
            max = dataType;
            return;
        }
        if ( dataType.getResult() < min.getResult() ) // vysledek dataTypu mensi nez vysledek min
            min = dataType;
        else if ( max.getResult() < dataType.getResult() ) // vysledek dataTypu vetsi nez vysledek max
            max = dataType;
    }

    /**
     * Metoda vraci formatovany string, na prvni radce je zapsane minumum, na druhe maximum
     * V pripade, kdy je min i max Null .. je vracen prazdny String
     * @return formatovany string
     */
    public String toString(){
        if ( min != null && max != null )
            return min.resultToString() + "\n" + max.resultToString();
        else
            return "";
    }

}
