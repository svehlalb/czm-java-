import Measurement.Measurement;

import java.io.*;

/**
 * Trida obsahuje main metodu, ktera ovlada praci se soubory (otevreni a zavreni) a logiku aplikace
 */
public class Application {
    /**
     * Metoda Main ovlada praci se soubory (otevreni a zavreni)
     * @param args - cesty k souborum, prvni String je vstupni soubor, druhy vystupni soubor
     */
    public static void main(String[] args)  {
        if ( args.length != 2 ){
            System.out.println("Chybný počet argumentů, opravte vstup.");
        }
        BufferedReader inFile = null;
        BufferedWriter outFile = null;
        try{
            inFile = new BufferedReader( new FileReader(args[0]));
            outFile = new BufferedWriter( new FileWriter(args[1]));
            Measurement measurementType = Measurement.getType(inFile, outFile); // vraci primo objekty typu A nebo B
            measurementType.readAndWrite();

        } catch (IOException e) {
            System.out.println( e.getMessage() + " -- Chyba při čtení souboru, zkontrolujte správnost zadaných argumentů a zkuste znovu.");
        } finally {
            if ( inFile != null ) {
                try {
                    inFile.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage() + " -- Vstupní soubor se nepovedlo uzavřít");
                }
            }

            if ( outFile != null) {
                try {
                    outFile.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage() + " -- Výstupní soubor se nepovedlo uzavřít");
                }
            }
        }
    }
}
