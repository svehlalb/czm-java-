### CZM příklad k příjímacímu řízení vývojářů

#### Spuštění programu
Příkaz ke spuštění programu: <br>
 
`java -jar app.jar inputFile outputFile` <br>

* Veškeré komentáře jsou uvedeny v češtině
* Struktura uspořádání tříd včetně dědičnosti je uvedena ve složce `Grafy`

##### Rozšiřitelnost
Pro rozšíření aplikace o další datový typ je potřeba:
 * Přidat nový label ve třídě DataType do pole `labelType` a v metodě `getType()` do switche
 * Vytvořit objekt, který bude implementovat rozhraní DataType a překryje metody `resultToString()` `getResult()` `getLabel()` <br>
 Pro rozšíření aplikace o další typ měření je potřeba:
 * Přidat nový typ ve třídě Measurement v metodě `getType()` do switche
 * Vytvořit objekt, který bude implimentovat rozhraní Measurement a překryje metody `readAndWrite()` <br>
##### Efektivita
* Pro načítání vstupu jsem použil třídu BufferedReader, která by měla být rychlejší než například FileReader
* Pro uchování minim a maxim u měření B jsem použil TreeMap, u kterého je hledání kliče O(log(n))
  
 


